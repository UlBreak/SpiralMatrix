import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int [][] matrix = new int[n][n];
        int p=0;
        int sum = 1;
        int endCondition = n*n;

        while(sum<=endCondition) {
            for (int i = p; i <= n - p-1; i++) {
                matrix[p][i] = sum;
                sum++;
            }
            for(int i=p+1;i<n-p;i++){
                matrix[i][n-1-p]=sum;
                sum++;
            }

            for(int i=n-p-2;i>=p;i--){
                matrix[n-p-1][i]=sum;
                sum++;
            }

            for(int i=n-p-2;i>p;i--){
                matrix[i][p]=sum;
                sum++;
            }
            p++;
        }



        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                System.out.printf("%3d ",matrix[i][j]);
            }
            System.out.println();
        }

    }
}
